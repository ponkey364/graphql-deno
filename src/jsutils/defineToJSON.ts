import { Class } from 'https://cdn.jsdelivr.net/gh/denoserverless/type-fest/mod.ts';
import nodejsCustomInspectSymbol from './nodejsCustomInspectSymbol.ts';

/**
 * The `defineToJSON()` function defines toJSON() and inspect() prototype
 * methods, if no function provided they become aliases for toString().
 */
export default function defineToJSON(
  classObject: Class<any> | ((...args: Array<any>) => any),
  fn: () => any = classObject.prototype.toString,
): void {
  classObject.prototype.toJSON = fn;
  classObject.prototype.inspect = fn;
  if (nodejsCustomInspectSymbol) {
    classObject.prototype[nodejsCustomInspectSymbol] = fn;
  }
}
