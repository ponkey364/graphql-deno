export type Path = {
  prev: Path | void,
  key: string | number,
};

/**
 * Given a Path and a key, return a new Path containing the new key.
 */
export function addPath(prev: Readonly<Path> | void, key: string | number) {
  return { prev, key };
}

/**
 * Given a Path, return an Array of the path keys.
 */
export function pathToArray(path: Readonly<Path>): Array<string | number> {
  const flattened = [];
  let curr: Path = path;
  while (curr) {
    flattened.push(curr.key);
    if (typeof curr.prev === "object") curr = curr.prev
  }
  return flattened.reverse();
}
