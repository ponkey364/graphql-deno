const nodejsCustomInspectSymbol = undefined // We're not using node, so make it undefined
  // typeof Symbol === 'function' && typeof Symbol.for === 'function'
  //   ? Symbol.for('nodejs.util.inspect.custom')
  //   : undefined;

export default nodejsCustomInspectSymbol;
