/**
 * A replacement for instanceof which includes an error warning when multi-realm
 * constructors are detected.
 */

export default function instanceOf(value: any, constructor: any) {
  return value instanceof constructor;
}
