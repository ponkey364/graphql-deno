/**
 * Returns true if the value acts like a Promise, i.e. has a "then" function,
 * otherwise returns false.
 */
// declare function isPromise(value: mixed): boolean %checks(value instanceof
//   Promise);

// eslint-disable-next-line no-redeclare
/**
 * 
 * @param {any} value 
 * @returns {boolean}
 */
export default function isPromise(value: any): boolean {
  return Boolean(value && typeof value.then === 'function');
}
